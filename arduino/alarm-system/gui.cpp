
#include <Arduino.h>

#include "custom.h"
#include "lcd-display.h"
#include "gui.h"
#include "keypad16.h"
#include "rfid-reader.h"

static void displayIntegerField(char *s, int len, int row, int col)
{
  Serial.println(s);
  lcd.setCursor(col, row);
  lcd.print(s);
}

static void displayStringField(char *s, int len, int row, int col)
{
  Serial.println(s);
  lcd.setCursor(col, row);
  lcd.print(s);
}

int Form::get(const char *question, char *entry, int len, void displayFunc(char *, int, int, int))
{
  int count = strlen(entry);
  char key;
  char copy[20];

  strcpy(copy, entry);
  lcd.clear();
  lcd.print(question);
  lcd.setCursor(0, 1);
  displayFunc(entry, count, 1, 0);
  while (1) {
    key = keypad.waitForKey();
    Serial.print(F("Form::get: "));
    Serial.println(key);
    switch (key) {
      case '#':
        if (count <= len) {
          lcd.clear();
          return strcmp(copy, entry) ? 1 : 0;
        }
        break;
      case '*':
        if (count > 0) {
          entry[--count] = ' ';
        }
        displayFunc(entry, len, 1, 0);
        break;
      default:
        if (count < len) {
          entry[count++] = key;
          entry[count] = 0;
        }
        else {
          memcpy(entry, entry + 1, len - 1);
          entry[len - 1] = key;
        }
        displayFunc(entry, len, 1, 0);
        break;
    }
  }
}

int Form::getBool(const char *question, char *entry)
{
  char key;
  char copy[20];

  strcpy(copy, entry);
  lcd.clear();
  lcd.print(question);
  lcd.setCursor(0, 1);
  displayIntegerField(entry, 1, 1, 0);
  while (1) {
    key = keypad.waitForKey();
    Serial.print(F("Form::getBool: "));
    Serial.println(key);
    switch (key) {
      case '#':
        lcd.clear();
        return strcmp(copy, entry) ? 1 : 0;
      case '0':
      case '1':
        entry[0] = key;
        entry[1] = 0;
        displayIntegerField(entry, 1, 1, 0);
        break;
      default:
        break;
    }
  }
}

int Form::getInteger(const char *question, int *i, const char *fmt)
{
  char buf[20];
  int status;

  sprintf(buf, fmt, *i);
  Serial.print(F("Form::getInteger: "));
  Serial.println(buf);
  status = this->get(question, buf, strlen(buf), displayIntegerField);
  if (status == true) {
    *i = atoi(buf);
  }
  return status;
}

int Form::getString(const char *question, char *s, int len)
{
  char buf[20];
  int status;

  strcpy(buf, s);
  Serial.print(F("Form::getString: "));
  Serial.println(buf);
  status = this->get(question, buf, strlen(buf), displayStringField);
  if (status == true) {
    strcpy(s, buf);
  }
  return status;
}

int Form::getBoolean(const char *question, int *i)
{
  char buf[2];
  int status;

  sprintf(buf, "%d", *i);
  Serial.print(F("Form::getBoolean: "));
  Serial.println(buf);
  status = this->getBool(question, buf);
  if (status == true) {
    *i = atoi(buf);
  }
  return status;
}

void enterAdminMode(void)
{
  Form form;
  int modified = 0;

  if (form.getInteger("DELAI D'ACTIVATION", &configData.activationDelay, "%03d") == true) {
    modified = true;
  }
  if (form.getInteger("DELAI D'INTRUSION", &configData.intrusionDelay, "%03d") == true) {
    modified = true;
  }
  if (form.getInteger("DUREE D'ALARME", &configData.alarmDuration, "%03d") == true) {
    modified = true;
  }
  if (form.getInteger("DUREE BACKLIGHT", &configData.backlightDuration, "%03d") == true) {
    modified = true;
  }
  if (form.getBoolean("AUTO REACTIVATION ?", &configData.autoReactivate) == true) {
    modified = true;
  }
  if (form.getString("ADMIN-CODE", configData.adminCode, LEN) == true) {
    modified = true;
  }
  if (form.getString("USER-CODE", configData.userCode, LEN) == true) {
    modified = true;
  }
#ifdef USE_MFRC522
  int answer = false;
  if (form.getBoolean("CHANGER ADMIN RFID ?", &answer) == true) {
    lcd.clear();
    lcd.print(F("PRESENTER RFID"));
    while (1) {
      uint32_t nuid = readNUID();
      if (nuid != 0) {
        Serial.print(F("NUID: 0x")); Serial.println(nuid, HEX);
        configData.adminNuid = nuid;
        modified = true;
        break;
      }
    }
  }
  answer = false;
  if (form.getBoolean("CHANGER USER RFID ?", &answer) == true) {
    lcd.clear();
    lcd.print(F("PRESENTER RFID"));
    while (1) {
      uint32_t nuid = readNUID();
      if (nuid != 0) {
        Serial.print(F("NUID: 0x")); Serial.println(nuid, HEX);
        configData.userNuid = nuid;
        modified = true;
        break;
      }
    }
#endif
  }
  lcd.clear();
  if (modified) {
    lcd.print(F("SAUVEGARDE"));
    saveConfig();
  }
  else {
    lcd.print(F("AUCUN CHANGEMENT"));
  }
  delay(1000);
  lcd.clear();
}
