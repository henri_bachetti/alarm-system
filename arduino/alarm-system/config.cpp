
#include <Arduino.h>
#include <EEPROM.h>

#include "config.h"

int state;
int intrusionsCnt;

struct config_data configData;

void saveConfig(void)
{
  configData.magic = MAGIC;
  EEPROM.put(CONFIG_ADDR, configData);
}

void restoreConfig(void)
{
  EEPROM.get(CONFIG_ADDR, configData);
  if (configData.magic != MAGIC) {
    Serial.println("no config available in EEPROM");
    resetConfig();
  }
  else {
    Serial.println("valid config data has been found in the EEPROM: ");
    Serial.print("magic: "); Serial.println(configData.magic, HEX);
    Serial.print("activation delay: "); Serial.println(configData.activationDelay);
    Serial.print("intrusion delay: "); Serial.println(configData.intrusionDelay);
    Serial.print("alarm duration: "); Serial.println(configData.alarmDuration);
    Serial.print("backlight duration: "); Serial.println(configData.backlightDuration);
    Serial.print("AUTO-REACTIVATE: "); Serial.println(configData.autoReactivate);
    Serial.print("ADMIN-CODE: "); Serial.println(configData.adminCode);
    Serial.print("USER-CODE: "); Serial.println(configData.userCode);
    Serial.print("ADMIN-RFID: 0x"); Serial.println(configData.adminNuid, HEX);
    Serial.print("USER-RFID: 0x"); Serial.println(configData.userNuid, HEX);
  }
}

void resetConfig(void)
{
  Serial.println("put default config in EEPROM");
  configData.activationDelay = ACTIVATION_DELAY;
  configData.intrusionDelay = INTRUSION_DELAY;
  configData.alarmDuration = ALARM_DURATION;
  configData.backlightDuration = BACKLIGHT_DURATION;
  configData.autoReactivate = AUTO_REACTIVATE;
  strcpy(configData.adminCode, ADMIN_CODE);
  strcpy(configData.userCode, USER_CODE);
  configData.adminNuid = ADMIN_NUID;
  configData.userNuid = USER_NUID;
  saveConfig();
}

void saveState(int state)
{
  struct state_data eepromData;

  eepromData.magic = MAGIC;
  eepromData.state = state;
  eepromData.intrusions = intrusionsCnt;
  EEPROM.put(STATE_ADDR, eepromData);
}

void restoreState(void)
{
  struct state_data data;

  EEPROM.get(STATE_ADDR, data);
  if (data.magic != MAGIC) {
    Serial.println("no state available in EEPROM");
    saveState(INACTIVE);
  }
  else {
    Serial.println("valid state data has been found in the EEPROM: ");
    Serial.print("magic: "); Serial.println(data.magic, HEX);
    Serial.print("state: "); Serial.println(data.state);
    Serial.print("intrusions: "); Serial.println(data.intrusions);
    state = data.state;
    intrusionsCnt = data.intrusions;
  }
}
