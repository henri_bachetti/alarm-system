
#ifndef _RFIDREADER_H_
#define _RFIDREADER_H_

#include "config.h"

void rfidInit(void);
uint32_t readNUID(void);

#endif
