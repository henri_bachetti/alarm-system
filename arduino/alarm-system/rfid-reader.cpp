
#include <SPI.h>
#include <MFRC522.h>

#include "rfid-reader.h"

MFRC522 rfid(RFID_SS_PIN, RFID_RST_PIN);
MFRC522::MIFARE_Key key;

void rfidInit(void)
{
  SPI.begin();
  rfid.PCD_Init();
  for (byte i = 0; i < 6; i++) {
    key.keyByte[i] = 0xFF;
  }
}

uint32_t readNUID(void)
{
  if (!rfid.PICC_IsNewCardPresent()) {
    return 0;
  }
  if (!rfid.PICC_ReadCardSerial()) {
    return 0;
  }
  Serial.print(F("PICC type: "));
  MFRC522::PICC_Type piccType = rfid.PICC_GetType(rfid.uid.sak);
  Serial.println(rfid.PICC_GetTypeName(piccType));
  if (piccType != MFRC522::PICC_TYPE_MIFARE_MINI &&
      piccType != MFRC522::PICC_TYPE_MIFARE_1K &&
      piccType != MFRC522::PICC_TYPE_MIFARE_4K) {
    Serial.println(F("Your tag is not of type MIFARE Classic."));
    return 0;
  }
  Serial.println(F("A new card has been detected."));
  rfid.PICC_HaltA();
  rfid.PCD_StopCrypto1();
  rfid.PICC_HaltA();
  rfid.PCD_StopCrypto1();
  return *(uint32_t *)rfid.uid.uidByte;
}
