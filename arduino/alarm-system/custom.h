
#ifndef _CUSTOM_H_
#define _CUSTOM_H_

#define USE_MFRC522

#define I2CADDR 0x20

#define LEN               8
#define RFID_SS_PIN       10
#define Z1                9
#define Z2                A0
#define RELAY             A1
#define RFID_RST_PIN      A2
#define LED               A3

// keypad
#define KBD_ACCORD

// if the on board relay is used
// of if a HIGH level relay module is used
#define RELAY_ON          HIGH
#define RELAY_OFF         LOW
// if a LOW level relay module is used
//#define RELAY_ON          LOW
//#define RELAY_OFF         HIGH

#define ADMIN_CODE        "123"
#define USER_CODE         "456"

#ifdef USE_MFRC522
#define ADMIN_NUID        0x55A185E9
#define USER_NUID         0xF80D4416
#endif

#endif
