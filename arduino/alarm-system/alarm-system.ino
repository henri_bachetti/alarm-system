
#include "config.h"
#include "lcd-display.h"
#include "keypad16.h"
#include "rfid-reader.h"
#include "gui.h"

unsigned long activationTime, intrusionTime, alarmTime;

void reset(void)
{
  keypadReset();
  printStatus(state);
}

void home(void)
{
  lcd.setCursor(0, 0);
  lcd.print(F("CODE: "));
  printStatus(state, true);
}

void setup() {
  Serial.begin(115200);
  restoreConfig();
  restoreState();
  pinMode(Z1, INPUT_PULLUP);
  pinMode(Z2, INPUT_PULLUP);
  pinMode(RELAY, OUTPUT);
  pinMode(LED, OUTPUT);
  if (state == ACTIVE) {
    digitalWrite(LED, HIGH);
  }
  digitalWrite(RELAY, RELAY_OFF);
#ifdef USE_MFRC522
  rfidInit();
#endif
  lcdInit();
  reset();
  char key = keypad.getKey();
  if (key) {
    Form form;
    int answer = false;

    if (form.getBoolean("RESET CONFIG ?", &answer) == true) {
      resetConfig();
      lcd.clear();
      lcd.print(F("RESET CONFIG OK"));
      delay(1000);
      lcd.clear();
    }
  }
  home();
}

void loop() {
  bool granted = false;

#ifdef USE_MFRC522
  uint32_t nuid = readNUID();
  if (nuid != 0) {
    Serial.print("NUID: 0x"); Serial.println(nuid, HEX);
    lcdBacklight(true);
    if (nuid == configData.adminNuid) {
      enterAdminMode();
      home();
    }
    else if (nuid == configData.userNuid) {
      printOK(true);
      granted = true;
    }
    else {
      printOK(false);
    }
    reset();
  }
#endif
  const char *code = keypadRead();
  if (code) {
    Serial.println(code);
    lcd.setCursor(6, 0);
    if (strcmp(code, configData.adminCode) == 0) {
      enterAdminMode();
      home();
    }
    else if (strcmp(code, configData.userCode) == 0) {
      printOK(true);
      granted = true;
    }
    else {
      printOK(false);
    }
    reset();
  }
  if (granted) {
    if (state == INACTIVE) {
      activationTime = millis();
      Serial.print("ACTIVATION "); Serial.println(activationTime);
      state = ACTIVATING;
    }
    else {
      Serial.println("DEACTIVATION");
      state = INACTIVE;
      printAlarm(ALARM_STOP);
      saveState(INACTIVE);
      digitalWrite(LED, LOW);
    }
    intrusionsCnt = 0;
  }
  lcdBacklight(false);
  printStatus(state);
  printIntrusions(intrusionsCnt);
  lcd.setCursor(14, 1);
  if (digitalRead(Z1) == HIGH) {
    if (state == ACTIVE) {
      lcdBacklight(true);
      alarmTime = millis();
      Serial.print(F("ALARM Z1 ")); Serial.println(alarmTime);
      state = ALARM_Z1;
      intrusionsCnt++;
      saveState(ACTIVE);
      digitalWrite(RELAY, RELAY_ON);
      printAlarm(ALARM_START);
    }
  }
  if (digitalRead(Z2) == HIGH) {
    if (state == ACTIVE) {
      lcdBacklight(true);
      intrusionTime = millis();
      Serial.print(F("INTRUSION ")); Serial.println(intrusionTime);
      state = INTRUSION;
    }
  }
  switch (state) {
    case INACTIVE:
      if (digitalRead(Z1) == HIGH) {
        printSensor("Z1");
      }
      else if (digitalRead(Z2) == HIGH) {
        printSensor("Z2");
      }
      else {
        printSensor("  ");
      }
      digitalWrite(RELAY, RELAY_OFF);
      break;
    case ACTIVATING:
      {
        unsigned long timeLeft = (activationDelay() - (millis() - activationTime)) / 1000;
        printTimeLeft(timeLeft);
        if (millis() - activationTime > activationDelay()) {
          Serial.println(F("ACTIVATED"));
          state = ACTIVE;
          intrusionsCnt = 0;
          saveState(ACTIVE);
          digitalWrite(LED, HIGH);
          printSensor("  ");
        }
        digitalWrite(RELAY, RELAY_OFF);
      }
      break;
    case INTRUSION:
      if (millis() - intrusionTime > intrusionDelay()) {
        alarmTime = millis();
        Serial.print(F("ALARM ")); Serial.println(alarmTime);
        state = ALARM_Z2;
        intrusionsCnt++;
        saveState(ACTIVE);
        digitalWrite(RELAY, RELAY_ON);
        printAlarm(ALARM_START);
      }
      else {
        unsigned long timeLeft = (intrusionDelay() - (millis() - intrusionTime)) / 1000;
        printTimeLeft(timeLeft);
      }
      break;
    case ALARM_Z2:
      if (millis() - alarmTime > alarmDuration()) {
        if (configData.autoReactivate) {
          state = ACTIVE;
        }
        else {
          state = INACTIVE;
        }
        digitalWrite(RELAY, RELAY_OFF);
        printAlarm(ALARM_STOP);
        printSensor("  ");
      }
      else {
        printAlarm(ALARM_CONT);
      }
      break;
    case ALARM_Z1:
      if (millis() - alarmTime > alarmDuration()) {
        state = INACTIVE;
        digitalWrite(RELAY, RELAY_OFF);
        printAlarm(ALARM_STOP);
        printSensor("  ");
      }
      else {
        printAlarm(ALARM_CONT);
      }
      break;
  }
}
