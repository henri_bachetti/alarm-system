
#include <Arduino.h>

#include "lcd-display.h"

LiquidCrystal_I2C lcd(0x27, LCD_COLS, LCD_ROWS);

uint8_t backSlash[8]  = {0x00, 0x10, 0x08, 0x04, 0x02, 0x01, 0x00, 0x00};

const char message1[] PROGMEM = "INACTIVE";
const char message2[] PROGMEM = "ACTIVATION";
const char message3[] PROGMEM = "ACTIVE";
const char message4[] PROGMEM = "INTRUSION";
const char message5[] PROGMEM = "ALARME Z2";
const char message6[] PROGMEM = "ALARME Z1";
const char * const messages[] PROGMEM = {message1, message2, message3, message4, message5, message6};

void lcdInit(void)
{
  lcd.begin();
  lcd.createChar(0, backSlash);
  lcdBacklight(true);
}

void lcdBacklight(bool activate)
{
  static unsigned long t;
  static bool active;

  if (activate) {
    active = true;
    t = millis();
    lcd.backlight();
    Serial.println("backlight ON");
  }
  if (active && millis() - t > backlightDuration()) {
    active = false;
    lcd.noBacklight();
    Serial.println("backlight OFF");
  }
}

void clearLine(int row)
{
  lcd.setCursor(0, row);
  lcd.print(F("                    "));
}

void printOK(bool yes)
{
  lcd.setCursor(6, 0);
  lcd.print(yes ? F("OK      ") : F("KO      "));
  delay(2000);
  lcd.setCursor(6, 0);
  lcd.print(F("        "));
}

void printStatus(int state, bool force)
{
  static int actual = -1;

  if (force || actual != state) {
    actual = state;
    char buf[LCD_COLS + 1];
    lcd.setCursor(0, 1);
    lcd.print(F("          "));
    lcd.setCursor(0, 1);
    lcd.print(strcpy_P(buf, (char*)pgm_read_word(&(messages[state]))));
  }
}

void printSensor(const char *status)
{
  lcd.setCursor(18, 1);
  lcd.print(status);
}

void printTimeLeft(int t)
{
  t++;
  lcd.setCursor(18, 1);
  if (t < 10) {
    lcd.print('0');
  }
  lcd.print(t);
}

void printRotatingChar(int how)
{
  static unsigned long t;
  static char c = '-';

  if (how == ALARM_STOP) {
    c = '-';
    lcd.setCursor(18, 1);
    lcd.print(F("  "));
    return;
  }
  if (how == ALARM_START) {
    t = millis();
  }
  if (millis() - t > 300) {
    lcd.setCursor(18, 1);
    lcd.print(c);
    lcd.print(c);
    switch (c) {
      case '-':
        c = '/';
        break;
      case '/':
        c = '|';
        break;
      case '|':
        c = 0;
        break;
      case 0:
        c = '-';
        break;
    }
    t = millis();
  }
}

void printBar(int how)
{
  static unsigned long t;
  static int col;
  unsigned long stepTime = alarmDuration() / (LCD_COLS+1);

  if (how == ALARM_STOP) {
    col = 0;
    clearLine(2);
    return;
  }
  if (how == ALARM_START) {
    t = millis();
    lcd.setCursor(0, 2);
    lcd.print(F("#                   "));
    col++;
  }
  if (millis() - t > stepTime) {
    if (col < 20) {
      lcd.setCursor(col++, 2);
      lcd.print('#');
    }
    t = millis();
  }
}

void printAlarm(int how)
{
  printRotatingChar(how);
  printBar(how);
}

void printIntrusions(int intrusionsCnt)
{
  static unsigned long t;
  static int actualCount;
  char buf[LCD_COLS + 1];

  sprintf(buf, "ALERTES         %4d", intrusionsCnt);
  if (intrusionsCnt == 0) {
    lcd.setCursor(0, 3);
    lcd.print(buf);
    return;
  }
  if (actualCount != intrusionsCnt) {
    t = millis();
    actualCount = intrusionsCnt;
  }
  if (millis() - t > 500) {
    digitalWrite(LED, HIGH);
    lcd.setCursor(0, 3);
    lcd.print(buf);
  }
  else {
    digitalWrite(LED, LOW);
    clearLine(3);
  }
  if (millis() - t > 1000) {
    t = millis();
  }
}
