
#ifndef _KEYPAD16_H_
#define _KEYPAD16_H_

#include <Keypad.h>

#include "config.h"
#include "custom.h"

#ifdef KBD_ACCORD
#define KBD_COL2            2
#define KBD_ROW1            3
#define KBD_COL1            4
#define KBD_ROW4            5
#define KBD_COL3            6
#define KBD_ROW3            7
#define KBD_ROW2            8
#else
#define KBD_ROW1            2
#define KBD_ROW2            3
#define KBD_ROW3            4
#define KBD_ROW4            5
#define KBD_COL1            6
#define KBD_COL2            7
#define KBD_COL3            8
#endif

void keypadInit(void);
void keypadReset(void);
const char *keypadRead(void);

extern Keypad keypad;

#endif
