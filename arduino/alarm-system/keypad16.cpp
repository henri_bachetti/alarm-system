
#include "keypad16.h"
#include "lcd-display.h"

const byte ROWS = 4; //four rows
const byte COLS = 3; //four columns
const char keys[ROWS][COLS] = {
  {'1', '2', '3'},
  {'4', '5', '6'},
  {'7', '8', '9'},
  {'*', '0', '#'}
};
byte rowPins[ROWS] = {KBD_ROW1, KBD_ROW2, KBD_ROW3, KBD_ROW4};
byte colPins[COLS] = {KBD_COL1, KBD_COL2, KBD_COL3};

Keypad keypad(makeKeymap(keys), rowPins, colPins, ROWS, COLS);

int cnt;
char digits[LEN + 1];

void keypadInit(void)
{
}

void keypadReset(void)
{
  memset(digits, 0, LEN);
  cnt = 0;
  lcd.setCursor(6, 0);
  lcd.print("        ");
}

const char *keypadRead(void)
{
  lcd.setCursor(6+cnt, 0);
  char key = keypad.getKey();
  if (key) {
    lcdBacklight(true);
    Serial.println(key);
    switch (key) {
      case '*':
        keypadReset();
        break;
      case '#':
        return digits;
        break;
      default:
        if (cnt < LEN) {
          digits[cnt++] = key;
          lcd.print(key);
        }
    }
  }
  return 0;
}
