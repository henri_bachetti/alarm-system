# Alarm System

The purpose of this page is to explain how to build an ARDUINO alarm system.

The board uses the following components :

- an ARDUINO NANO
- a matrix keypad
- an RC522 RFID reader (option)
- a 20x4 LCD display
- a relay module
- a PIR sensor
- a power 5V supply

### ELECTRONICS

The schematics are made using KICAD 5.1.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2022/03/arduino-centrale-dalarme.html

