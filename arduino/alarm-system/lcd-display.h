
#ifndef _LCDDISPLAY_H_
#define _LCDDISPLAY_H_

#include <Wire.h>
#include <LiquidCrystal_I2C.h>

#include "config.h"

#define LCD_COLS    20
#define LCD_ROWS    4

#define ALARM_CONT        0
#define ALARM_START       1
#define ALARM_STOP        2

void lcdInit(void);
void lcdBacklight(bool activate=false);
void clearLine(int row);
void printOK(bool yes);
void printStatus(int state, bool force=false);
void printSensor(const char *status);
void printTimeLeft(int t);
void printRotatingChar(int how);
void printBar(int how);
void printAlarm(int how);
void printIntrusions(int intrusionsCnt);

extern LiquidCrystal_I2C lcd;

#endif
