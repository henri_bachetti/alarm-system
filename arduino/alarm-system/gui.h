
#ifndef _GUI_H_
#define _GUI_H_

class Form
{
  public:
    int get(const char *question, char *entry, int len, void displayFunc(char *, int, int, int));
    int getBool(const char *question, char *entry);
    int getInteger(const char *question, int *i, const char *fmt);
    int getString(const char *question, char *s, int len);
    int getBoolean(const char *question, int *i);
};

void enterAdminMode(void);

#endif
