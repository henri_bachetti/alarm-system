
#ifndef _CONFIG_H_
#define _CONFIG_H_

#include "custom.h"

#define ACTIVATION_DELAY    5
#define INTRUSION_DELAY     10
#define ALARM_DURATION      12
#define BACKLIGHT_DURATION  30
#define AUTO_REACTIVATE     true

struct config_data
{
  int activationDelay;
  int intrusionDelay;
  int alarmDuration;
  int backlightDuration;
  int autoReactivate;
  char adminCode[LEN+1];
  char userCode[LEN+1];
  uint32_t adminNuid;
  uint32_t userNuid;
  unsigned long magic;
};

#define INACTIVE          0
#define ACTIVATING        1
#define ACTIVE            2
#define INTRUSION         3
#define ALARM_Z2          4
#define ALARM_Z1          5

struct state_data
{
  int state;
  int intrusions;
  unsigned long magic;
};

#define MAGIC             0xDEADBEEF
#define CONFIG_ADDR       0
#define STATE_ADDR        CONFIG_ADDR + sizeof (struct config_data)

extern int state;
extern int intrusionsCnt;

extern struct config_data configData;

void saveConfig(void);
void restoreConfig(void);
void resetConfig(void);
void saveState(int state);
void restoreState(void);
inline unsigned long activationDelay(void) {
  return configData.activationDelay * 1000L;
}
inline unsigned long intrusionDelay(void) {
  return configData.intrusionDelay * 1000L;
}
inline unsigned long alarmDuration(void) {
  return configData.alarmDuration * 1000L;
}
inline unsigned long backlightDuration(void) {
  return configData.backlightDuration * 1000L;
}

#endif
